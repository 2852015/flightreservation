/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain;

import java.time.ZonedDateTime;

/**
 *
 * @author Room107
 */
public class Trip {
    private AirPort fromairport;
    private AirPort toAirport;
    private byte noOfPassengers;
    private ZonedDateTime departureDateTime;
    private ZonedDateTime returnDateTime;
    private boolean directFlight;
    private TravelClass travelClass;
}


